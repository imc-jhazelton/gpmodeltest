package com.example.test;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class GPModel {

	@Id
	@Column(name = "DEX_ROW_ID")
	Integer dexRowId;

	@Column(name = "VCHRNMBR")
	String vchrnmbr;

	@Column(name = "VENDORID")
	String vendorId;

	@Column(name = "DOCTYPE")
	Integer docType;

	@Column(name = "DOCDATE")
	LocalDateTime docDate;

	@Column(name = "DOCNUMBR")
	String docNumbr;

	@Column(name = "DOCAMNT")
	Double docAmnt;

	@Column(name = "CURTRXAM")
	Double curtrxam;

	@Column(name = "DISTKNAM")
	Double distknam;

	@Column(name = "DISCAMNT")
	Double discamnt;

	@Column(name = "DSCDLRAM")
	Double dscdlram;

	@Column(name = "BACHNUMB")
	String bachnumb;

	@Column(name = "TRXSORCE")
	String trxsorce;

	@Column(name = "BCHSOURC")
	String bchsourc;

	@Column(name = "DISCDATE")
	LocalDateTime discDate;

	@Column(name = "DUEDATE")
	LocalDateTime dueDate;

	@Column(name = "PORDNMBR")
	String pordnmbr;

	@Column(name = "TEN99AMNT")
	Double ten99amnt;

	@Column(name = "WROFAMNT")
	Double wrofamnt;

	@Column(name = "DISAMTAV")
	Double discamtav;

	@Column(name = "TRXDSCRN")
	String trxdscrn;

	@Column(name = "UN1099AM")
	Double un1099am;

	@Column(name = "BKTPURAM")
	Double bktpuram;

	@Column(name = "BKTFRTAM")
	Double bktfrtam;

	@Column(name = "BKTMSCAM")
	Double bktmscam;

	@Column(name = "VOIDED")
	Integer voided;

	@Column(name = "HOLD")
	Integer hold;

	@Column(name = "CHEKBKID")
	String chekbkid;

	@Column(name = "DINVPDOF")
	LocalDateTime dinvpdof;

	@Column(name = "PPSAMDED")
	Double ppsdmfed;

	@Column(name = "PPSTAXRT")
	Integer ppstaxrt;

	@Column(name = "PGRAMSBJ")
	Integer pgramsbj;

	@Column(name = "GSTDSAMT")
	Double gstdsamt;

	@Column(name = "POSTEDDT")
	LocalDateTime posteddt;

	@Column(name = "PTDUSRID")
	String ptdusrid;

	@Column(name = "MODIFDT")
	LocalDateTime modifdt;

	@Column(name = "MDFUSRID")
	String mdfusrid;

	@Column(name = "PYENTTYP")
	Integer pyenttyp;

	@Column(name = "CARDNAME")
	String cardname;

	@Column(name = "PRCHAMNT")
	Double prchamnt;

	@Column(name = "TRDISAMT")
	Double trdisamt;

	@Column(name = "MSCCHAMT")
	Double mscchamt;

	@Column(name = "FRTAMNT")
	Double frtamnt;

	@Column(name = "TAXAMNT")
	Double taxamnt;

	@Column(name = "TTLPYMTS")
	Double ttlpymts;

	@Column(name = "CURNCYID")
	String curncyid;

	@Column(name = "PYMTRMID")
	String pymtrmid;

	@Column(name = "SHIPMTHD")
	String shipmthd;

	@Column(name = "TAXSCHID")
	String taxschid;

	@Column(name = "PCHSCHID")
	String pchschid;

	@Column(name = "FRTSCHID")
	String frtschid;

	@Column(name = "MSCSCHID")
	String mscschid;

	@Column(name = "PSTGDATE")
	LocalDateTime pstgdate;

	@Column(name = "DISAVTKN")
	Double disavtkn;

	@Column(name = "CNTRLTYP")
	Integer cntrltyp;

	@Column(name = "NOTEINDX")
	Double noteindx;

	@Column(name = "PRCTDISC")
	Integer prctdisc;

	@Column(name = "RETNAGAM")
	Integer retnagam;

	@Column(name = "ICTRX")
	Integer ictrx;

	@Column(name = "TAX_DATE")
	LocalDateTime taxDate;

	@Column(name = "PRCHDATE")
	LocalDateTime prchdate;

	@Column(name = "CORRCTN")
	Integer corrctn;

	@Column(name = "SIMPLIFD")
	Integer simplifd;

	@Column(name = "BNKRCAMT")
	Double bnkrcamt;

	@Column(name = "APLYWITH")
	Integer aplywith;

	@Column(name = "Electronic")
	Integer electronic;

	@Column(name = "ECTRX")
	Integer ectrx;

	@Column(name = "DocPrinted")
	Integer docprinted;

	@Column(name = "TaxInvReqd")
	Integer taxinvreqd;

	@Column(name = "VNDCHKNM")
	String vndchknm;

	@Column(name = "BackoutTradeDisc")
	Double backoutradedisc;

	@Column(name = "CBVAT")
	Integer cbvat;

	@Column(name = "VADCDTRO")
	String vadcdtro;

	@Column(name = "TEN99TYPE")
	Integer ten99type;

	@Column(name = "TEN99BOXNUMBER")
	Integer ten99boxnumber;

	@Column(name = "PONUMBER")
	String ponumber;

	@Column(name = "Workflow_Status")
	Integer workflowStatus;

	@Column(name = "InvoiceReceiptDate")
	LocalDateTime invoiceReceiptDate;

	@Column(name = "DEX_ROW_TS")
	LocalDateTime dexrowTs;

	public Integer getDexRowId() {
		return dexRowId;
	}

	public void setDexRowId(Integer dexRowId) {
		this.dexRowId = dexRowId;
	}

	public String getVchrnmbr() {
		return vchrnmbr;
	}

	public void setVchrnmbr(String vchrnmbr) {
		this.vchrnmbr = vchrnmbr;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public Integer getDocType() {
		return docType;
	}

	public void setDocType(Integer docType) {
		this.docType = docType;
	}

	public LocalDateTime getDocDate() {
		return docDate;
	}

	public void setDocDate(LocalDateTime docDate) {
		this.docDate = docDate;
	}

	public String getDocNumbr() {
		return docNumbr;
	}

	public void setDocNumbr(String docNumbr) {
		this.docNumbr = docNumbr;
	}

	public Double getDocAmnt() {
		return docAmnt;
	}

	public void setDocAmnt(Double docAmnt) {
		this.docAmnt = docAmnt;
	}

	public Double getCurtrxam() {
		return curtrxam;
	}

	public void setCurtrxam(Double curtrxam) {
		this.curtrxam = curtrxam;
	}

	public Double getDistknam() {
		return distknam;
	}

	public void setDistknam(Double distknam) {
		this.distknam = distknam;
	}

	public Double getDiscamnt() {
		return discamnt;
	}

	public void setDiscamnt(Double discamnt) {
		this.discamnt = discamnt;
	}

	public Double getDscdlram() {
		return dscdlram;
	}

	public void setDscdlram(Double dscdlram) {
		this.dscdlram = dscdlram;
	}

	public String getBachnumb() {
		return bachnumb;
	}

	public void setBachnumb(String bachnumb) {
		this.bachnumb = bachnumb;
	}

	public String getTrxsorce() {
		return trxsorce;
	}

	public void setTrxsorce(String trxsorce) {
		this.trxsorce = trxsorce;
	}

	public String getBchsourc() {
		return bchsourc;
	}

	public void setBchsourc(String bchsourc) {
		this.bchsourc = bchsourc;
	}

	public LocalDateTime getDiscDate() {
		return discDate;
	}

	public void setDiscDate(LocalDateTime discDate) {
		this.discDate = discDate;
	}

	public LocalDateTime getDueDate() {
		return dueDate;
	}

	public void setDueDate(LocalDateTime dueDate) {
		this.dueDate = dueDate;
	}

	public String getPordnmbr() {
		return pordnmbr;
	}

	public void setPordnmbr(String pordnmbr) {
		this.pordnmbr = pordnmbr;
	}

	public Double getTen99amnt() {
		return ten99amnt;
	}

	public void setTen99amnt(Double ten99amnt) {
		this.ten99amnt = ten99amnt;
	}

	public Double getWrofamnt() {
		return wrofamnt;
	}

	public void setWrofamnt(Double wrofamnt) {
		this.wrofamnt = wrofamnt;
	}

	public Double getDiscamtav() {
		return discamtav;
	}

	public void setDiscamtav(Double discamtav) {
		this.discamtav = discamtav;
	}

	public String getTrxdscrn() {
		return trxdscrn;
	}

	public void setTrxdscrn(String trxdscrn) {
		this.trxdscrn = trxdscrn;
	}

	public Double getUn1099am() {
		return un1099am;
	}

	public void setUn1099am(Double un1099am) {
		this.un1099am = un1099am;
	}

	public Double getBktpuram() {
		return bktpuram;
	}

	public void setBktpuram(Double bktpuram) {
		this.bktpuram = bktpuram;
	}

	public Double getBktfrtam() {
		return bktfrtam;
	}

	public void setBktfrtam(Double bktfrtam) {
		this.bktfrtam = bktfrtam;
	}

	public Double getBktmscam() {
		return bktmscam;
	}

	public void setBktmscam(Double bktmscam) {
		this.bktmscam = bktmscam;
	}

	public Integer getVoided() {
		return voided;
	}

	public void setVoided(Integer voided) {
		this.voided = voided;
	}

	public Integer getHold() {
		return hold;
	}

	public void setHold(Integer hold) {
		this.hold = hold;
	}

	public String getChekbkid() {
		return chekbkid;
	}

	public void setChekbkid(String chekbkid) {
		this.chekbkid = chekbkid;
	}

	public LocalDateTime getDinvpdof() {
		return dinvpdof;
	}

	public void setDinvpdof(LocalDateTime dinvpdof) {
		this.dinvpdof = dinvpdof;
	}

	public Double getPpsdmfed() {
		return ppsdmfed;
	}

	public void setPpsdmfed(Double ppsdmfed) {
		this.ppsdmfed = ppsdmfed;
	}

	public Integer getPpstaxrt() {
		return ppstaxrt;
	}

	public void setPpstaxrt(Integer ppstaxrt) {
		this.ppstaxrt = ppstaxrt;
	}

	public Integer getPgramsbj() {
		return pgramsbj;
	}

	public void setPgramsbj(Integer pgramsbj) {
		this.pgramsbj = pgramsbj;
	}

	public Double getGstdsamt() {
		return gstdsamt;
	}

	public void setGstdsamt(Double gstdsamt) {
		this.gstdsamt = gstdsamt;
	}

	public LocalDateTime getPosteddt() {
		return posteddt;
	}

	public void setPosteddt(LocalDateTime posteddt) {
		this.posteddt = posteddt;
	}

	public String getPtdusrid() {
		return ptdusrid;
	}

	public void setPtdusrid(String ptdusrid) {
		this.ptdusrid = ptdusrid;
	}

	public LocalDateTime getModifdt() {
		return modifdt;
	}

	public void setModifdt(LocalDateTime modifdt) {
		this.modifdt = modifdt;
	}

	public String getMdfusrid() {
		return mdfusrid;
	}

	public void setMdfusrid(String mdfusrid) {
		this.mdfusrid = mdfusrid;
	}

	public Integer getPyenttyp() {
		return pyenttyp;
	}

	public void setPyenttyp(Integer pyenttyp) {
		this.pyenttyp = pyenttyp;
	}

	public String getCardname() {
		return cardname;
	}

	public void setCardname(String cardname) {
		this.cardname = cardname;
	}

	public Double getPrchamnt() {
		return prchamnt;
	}

	public void setPrchamnt(Double prchamnt) {
		this.prchamnt = prchamnt;
	}

	public Double getTrdisamt() {
		return trdisamt;
	}

	public void setTrdisamt(Double trdisamt) {
		this.trdisamt = trdisamt;
	}

	public Double getMscchamt() {
		return mscchamt;
	}

	public void setMscchamt(Double mscchamt) {
		this.mscchamt = mscchamt;
	}

	public Double getFrtamnt() {
		return frtamnt;
	}

	public void setFrtamnt(Double frtamnt) {
		this.frtamnt = frtamnt;
	}

	public Double getTaxamnt() {
		return taxamnt;
	}

	public void setTaxamnt(Double taxamnt) {
		this.taxamnt = taxamnt;
	}

	public Double getTtlpymts() {
		return ttlpymts;
	}

	public void setTtlpymts(Double ttlpymts) {
		this.ttlpymts = ttlpymts;
	}

	public String getCurncyid() {
		return curncyid;
	}

	public void setCurncyid(String curncyid) {
		this.curncyid = curncyid;
	}

	public String getPymtrmid() {
		return pymtrmid;
	}

	public void setPymtrmid(String pymtrmid) {
		this.pymtrmid = pymtrmid;
	}

	public String getShipmthd() {
		return shipmthd;
	}

	public void setShipmthd(String shipmthd) {
		this.shipmthd = shipmthd;
	}

	public String getTaxschid() {
		return taxschid;
	}

	public void setTaxschid(String taxschid) {
		this.taxschid = taxschid;
	}

	public String getPchschid() {
		return pchschid;
	}

	public void setPchschid(String pchschid) {
		this.pchschid = pchschid;
	}

	public String getFrtschid() {
		return frtschid;
	}

	public void setFrtschid(String frtschid) {
		this.frtschid = frtschid;
	}

	public String getMscschid() {
		return mscschid;
	}

	public void setMscschid(String mscschid) {
		this.mscschid = mscschid;
	}

	public LocalDateTime getPstgdate() {
		return pstgdate;
	}

	public void setPstgdate(LocalDateTime pstgdate) {
		this.pstgdate = pstgdate;
	}

	public Double getDisavtkn() {
		return disavtkn;
	}

	public void setDisavtkn(Double disavtkn) {
		this.disavtkn = disavtkn;
	}

	public Integer getCntrltyp() {
		return cntrltyp;
	}

	public void setCntrltyp(Integer cntrltyp) {
		this.cntrltyp = cntrltyp;
	}

	public Double getNoteindx() {
		return noteindx;
	}

	public void setNoteindx(Double noteindx) {
		this.noteindx = noteindx;
	}

	public Integer getPrctdisc() {
		return prctdisc;
	}

	public void setPrctdisc(Integer prctdisc) {
		this.prctdisc = prctdisc;
	}

	public Integer getRetnagam() {
		return retnagam;
	}

	public void setRetnagam(Integer retnagam) {
		this.retnagam = retnagam;
	}

	public Integer getIctrx() {
		return ictrx;
	}

	public void setIctrx(Integer ictrx) {
		this.ictrx = ictrx;
	}

	public LocalDateTime getTaxDate() {
		return taxDate;
	}

	public void setTaxDate(LocalDateTime taxDate) {
		this.taxDate = taxDate;
	}

	public LocalDateTime getPrchdate() {
		return prchdate;
	}

	public void setPrchdate(LocalDateTime prchdate) {
		this.prchdate = prchdate;
	}

	public Integer getCorrctn() {
		return corrctn;
	}

	public void setCorrctn(Integer corrctn) {
		this.corrctn = corrctn;
	}

	public Integer getSimplifd() {
		return simplifd;
	}

	public void setSimplifd(Integer simplifd) {
		this.simplifd = simplifd;
	}

	public Double getBnkrcamt() {
		return bnkrcamt;
	}

	public void setBnkrcamt(Double bnkrcamt) {
		this.bnkrcamt = bnkrcamt;
	}

	public Integer getAplywith() {
		return aplywith;
	}

	public void setAplywith(Integer aplywith) {
		this.aplywith = aplywith;
	}

	public Integer getElectronic() {
		return electronic;
	}

	public void setElectronic(Integer electronic) {
		this.electronic = electronic;
	}

	public Integer getEctrx() {
		return ectrx;
	}

	public void setEctrx(Integer ectrx) {
		this.ectrx = ectrx;
	}

	public Integer getDocprinted() {
		return docprinted;
	}

	public void setDocprinted(Integer docprinted) {
		this.docprinted = docprinted;
	}

	public Integer getTaxinvreqd() {
		return taxinvreqd;
	}

	public void setTaxinvreqd(Integer taxinvreqd) {
		this.taxinvreqd = taxinvreqd;
	}

	public String getVndchknm() {
		return vndchknm;
	}

	public void setVndchknm(String vndchknm) {
		this.vndchknm = vndchknm;
	}

	public Double getBackoutradedisc() {
		return backoutradedisc;
	}

	public void setBackoutradedisc(Double backoutradedisc) {
		this.backoutradedisc = backoutradedisc;
	}

	public Integer getCbvat() {
		return cbvat;
	}

	public void setCbvat(Integer cbvat) {
		this.cbvat = cbvat;
	}

	public String getVadcdtro() {
		return vadcdtro;
	}

	public void setVadcdtro(String vadcdtro) {
		this.vadcdtro = vadcdtro;
	}

	public Integer getTen99type() {
		return ten99type;
	}

	public void setTen99type(Integer ten99type) {
		this.ten99type = ten99type;
	}

	public Integer getTen99boxnumber() {
		return ten99boxnumber;
	}

	public void setTen99boxnumber(Integer ten99boxnumber) {
		this.ten99boxnumber = ten99boxnumber;
	}

	public String getPonumber() {
		return ponumber;
	}

	public void setPonumber(String ponumber) {
		this.ponumber = ponumber;
	}

	public Integer getWorkflowStatus() {
		return workflowStatus;
	}

	public void setWorkflowStatus(Integer workflowStatus) {
		this.workflowStatus = workflowStatus;
	}

	public LocalDateTime getInvoiceReceiptDate() {
		return invoiceReceiptDate;
	}

	public void setInvoiceReceiptDate(LocalDateTime invoiceReceiptDate) {
		this.invoiceReceiptDate = invoiceReceiptDate;
	}

	public LocalDateTime getDexrowTs() {
		return dexrowTs;
	}

	public void setDexrowTs(LocalDateTime dexrowTs) {
		this.dexrowTs = dexrowTs;
	}

	@Override
	public String toString() {
		return "GPModel [dexRowId=" + dexRowId + ", vchrnmbr=" + vchrnmbr + ", vendorId=" + vendorId + ", docType="
				+ docType + ", docDate=" + docDate + ", docNumbr=" + docNumbr + ", docAmnt=" + docAmnt + ", curtrxam="
				+ curtrxam + ", distknam=" + distknam + ", discamnt=" + discamnt + ", dscdlram=" + dscdlram
				+ ", bachnumb=" + bachnumb + ", trxsorce=" + trxsorce + ", bchsourc=" + bchsourc + ", discDate="
				+ discDate + ", dueDate=" + dueDate + ", pordnmbr=" + pordnmbr + ", ten99amnt=" + ten99amnt
				+ ", wrofamnt=" + wrofamnt + ", discamtav=" + discamtav + ", trxdscrn=" + trxdscrn + ", un1099am="
				+ un1099am + ", bktpuram=" + bktpuram + ", bktfrtam=" + bktfrtam + ", bktmscam=" + bktmscam
				+ ", voided=" + voided + ", hold=" + hold + ", chekbkid=" + chekbkid + ", dinvpdof=" + dinvpdof
				+ ", ppsdmfed=" + ppsdmfed + ", ppstaxrt=" + ppstaxrt + ", pgramsbj=" + pgramsbj + ", gstdsamt="
				+ gstdsamt + ", posteddt=" + posteddt + ", ptdusrid=" + ptdusrid + ", modifdt=" + modifdt
				+ ", mdfusrid=" + mdfusrid + ", pyenttyp=" + pyenttyp + ", cardname=" + cardname + ", prchamnt="
				+ prchamnt + ", trdisamt=" + trdisamt + ", mscchamt=" + mscchamt + ", frtamnt=" + frtamnt + ", taxamnt="
				+ taxamnt + ", ttlpymts=" + ttlpymts + ", curncyid=" + curncyid + ", pymtrmid=" + pymtrmid
				+ ", shipmthd=" + shipmthd + ", taxschid=" + taxschid + ", pchschid=" + pchschid + ", frtschid="
				+ frtschid + ", mscschid=" + mscschid + ", pstgdate=" + pstgdate + ", disavtkn=" + disavtkn
				+ ", cntrltyp=" + cntrltyp + ", noteindx=" + noteindx + ", prctdisc=" + prctdisc + ", retnagam="
				+ retnagam + ", ictrx=" + ictrx + ", taxDate=" + taxDate + ", prchdate=" + prchdate + ", corrctn="
				+ corrctn + ", simplifd=" + simplifd + ", bnkrcamt=" + bnkrcamt + ", aplywith=" + aplywith
				+ ", electronic=" + electronic + ", ectrx=" + ectrx + ", docprinted=" + docprinted + ", taxinvreqd="
				+ taxinvreqd + ", vndchknm=" + vndchknm + ", backoutradedisc=" + backoutradedisc + ", cbvat=" + cbvat
				+ ", vadcdtro=" + vadcdtro + ", ten99type=" + ten99type + ", ten99boxnumber=" + ten99boxnumber
				+ ", ponumber=" + ponumber + ", workflowStatus=" + workflowStatus + ", invoiceReceiptDate="
				+ invoiceReceiptDate + ", dexrowTs=" + dexrowTs + "]";
	}

}
