package com.example.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CreateEntityFields {

	static HashMap<String, String> jdbcMappings = new HashMap<>();

	static {
		jdbcMappings.put("[char]", "String");
		jdbcMappings.put("[smallint]", "Short");
		jdbcMappings.put("[datetime]", "java.sql.Timestamp");
		jdbcMappings.put("[numeric]", "java.math.BigDecimal");
		jdbcMappings.put("[tinyint]", "Short");
		jdbcMappings.put("[tinyint]", "Integer");
	}

	public static void main(String[] args) {
		try {
			List<String> lines = Files.readAllLines(Paths.get("model.sql"));
			List<String> fieldLines = IntStream.range(0, lines.size()).filter(i -> i > 10 && i < 90)
					.mapToObj(i -> lines.get(i)).collect(Collectors.toList());

			String formattedLine = "";
			for (String line : fieldLines) {
				String[] lineSplit = line.split(" ");
				System.out.println(Arrays.asList(lineSplit));
				formattedLine = "@Column(name = \"" + lineSplit[0].replaceAll("\\p{P}","").trim() + "\")\n" 
				+ "private " + jdbcMappings.get(lineSplit[1]);
				
				System.out.println(formattedLine);
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

//	System.out.println(Arrays.asList(lines.get(11).split(" "))
//			.stream()
//			.map(j -> {
//				String formattedString = "";
//				formattedString += 
//				return j.trim();
//			})
//			.collect(Collectors.toList()));
}
