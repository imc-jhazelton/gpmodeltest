package com.example.test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.test.repository.ais.AISPM20000Repository;
import com.example.test.repository.ais.AISPM30200Repository;
import com.example.test.repository.dnj.DNJPM20000Repository;
import com.example.test.repository.dnj.DNJPM30200Repository;
import com.example.test.repository.gis.GISPM20000Repository;
import com.example.test.repository.gis.GISPM30200Repository;
import com.example.test.repository.glso.GLSOPM20000Repository;
import com.example.test.repository.glso.GLSOPM30200Repository;
import com.example.test.repository.hmis.HMISPM20000Repository;
import com.example.test.repository.hmis.HMISPM30200Repository;
import com.example.test.repository.imc.IMCPM20000Repository;
import com.example.test.repository.imc.IMCPM30200Repository;
import com.example.test.repository.pds.PDSPM20000Repository;
import com.example.test.repository.pds.PDSPM30200Repository;
import com.example.test.repository.pts.PTSPM20000Repository;
import com.example.test.repository.pts.PTSPM30200Repository;
import com.example.test.tablemodels.Pm20000TableModel;

@RestController
@RequestMapping(value = "/")
public class MainController {

	@Autowired
	AISPM20000Repository aisPM20000Repository;
	@Autowired
	AISPM30200Repository aisPM30200Repository;
	
	@Autowired
	DNJPM20000Repository dnjPM20000Repository;
	@Autowired
	DNJPM30200Repository dnjPM30200Repository;
	
	@Autowired
	GISPM20000Repository gisPM20000Repository;
	@Autowired
	GISPM30200Repository gisPM30200Repository;
	
	@Autowired
	GLSOPM20000Repository glsoPM20000Repository;
	@Autowired
	GLSOPM30200Repository glsoPM30200Repository;
	
	@Autowired
	HMISPM20000Repository hmisPM20000Repository;
	@Autowired
	HMISPM30200Repository hmisPM30200Repository;
	
	@Autowired
	IMCPM20000Repository imcPM20000Repository;
	@Autowired
	IMCPM30200Repository imcPM30200Repository;
	
	@Autowired
	PDSPM20000Repository pdsPM20000Repository;
	@Autowired
	PDSPM30200Repository pdsPM30200Repository;
	
	@Autowired
	PTSPM20000Repository ptsPM20000Repository;
	@Autowired
	PTSPM30200Repository ptsPM30200Repository;
	
	@GetMapping
	public String getInfo() {
		return "Hello There!";
	}
	
	@GetMapping("searchAllCompanyAP")
	public AllCompanyAPResponse searchAllCompanyAP(@RequestParam String vendorid) {
		AllCompanyAPResponse response = new AllCompanyAPResponse();
		
		response.setAis((List<Pm20000TableModel>) aisPM20000Repository.findAllByVendorid(vendorid));
		response.setDnj((List<Pm20000TableModel>) dnjPM20000Repository.findAllByVendorid(vendorid));
		response.setGis((List<Pm20000TableModel>) gisPM20000Repository.findAllByVendorid(vendorid));
		response.setGlso((List<Pm20000TableModel>) glsoPM20000Repository.findAllByVendorid(vendorid));
		response.setHmis((List<Pm20000TableModel>) hmisPM20000Repository.findAllByVendorid(vendorid));
		response.setImc((List<Pm20000TableModel>) imcPM20000Repository.findAllByVendorid(vendorid));
		response.setPds((List<Pm20000TableModel>) pdsPM20000Repository.findAllByVendorid(vendorid));
		response.setPts((List<Pm20000TableModel>) ptsPM20000Repository.findAllByVendorid(vendorid));
		
		return response;
	}
	
	



}
