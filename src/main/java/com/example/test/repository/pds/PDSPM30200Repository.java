package com.example.test.repository.pds;

import javax.persistence.PersistenceContext;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.test.MasterRepository;
import com.example.test.tablemodels.Pm30200TableModel;

@Repository
@PersistenceContext(name = "pdsEntityManagerFactory")
public interface PDSPM30200Repository extends JpaRepository<Pm30200TableModel, Integer>, MasterRepository {

}
