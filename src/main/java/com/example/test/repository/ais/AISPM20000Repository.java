package com.example.test.repository.ais;

import javax.persistence.PersistenceContext;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.test.MasterRepository;
import com.example.test.tablemodels.Pm20000TableModel;

@Repository
@PersistenceContext(name = "aisEntityManagerFactory")
public interface AISPM20000Repository extends JpaRepository<Pm20000TableModel, Integer>, MasterRepository {

}
