package com.example.test.repository.glso;

import javax.persistence.PersistenceContext;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.test.MasterRepository;
import com.example.test.tablemodels.Pm20000TableModel;

@Repository
@PersistenceContext(name = "glsoEntityManagerFactory")
public interface GLSOPM20000Repository extends JpaRepository<Pm20000TableModel, Integer>, MasterRepository {

}
