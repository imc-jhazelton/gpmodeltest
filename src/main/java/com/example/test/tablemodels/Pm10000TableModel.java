package com.example.test.tablemodels;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PM10000", schema = "dbo")
public class Pm10000TableModel {

  @Column(name = "BACHNUMB")
  private String bachnumb;
  @Column(name = "BCHSOURC")
  private String bchsourc;
  @Column(name = "VCHNUMWK")
  private String vchnumwk;
  @Column(name = "VENDORID")
  private String vendorid;
  @Column(name = "DOCNUMBR")
  private String docnumbr;
  @Column(name = "DOCTYPE")
  private long doctype;
  @Column(name = "SOURCDOC")
  private String sourcdoc;
  @Column(name = "DOCAMNT")
  private String docamnt;
  @Column(name = "DOCDATE")
  private java.sql.Timestamp docdate;
  @Column(name = "PSTGDATE")
  private java.sql.Timestamp pstgdate;
  @Column(name = "VADDCDPR")
  private String vaddcdpr;
  @Column(name = "VADCDTRO")
  private String vadcdtro;
  @Column(name = "PYMTRMID")
  private String pymtrmid;
  @Column(name = "TAXSCHID")
  private String taxschid;
  @Column(name = "DUEDATE")
  private java.sql.Timestamp duedate;
  @Column(name = "DSCDLRAM")
  private String dscdlram;
  @Column(name = "DISCDATE")
  private java.sql.Timestamp discdate;
  @Column(name = "PRCHAMNT")
  private String prchamnt;
  @Column(name = "CHRGAMNT")
  private String chrgamnt;
  @Column(name = "CASHAMNT")
  private String cashamnt;
  @Column(name = "CAMCBKID")
  private String camcbkid;
  @Column(name = "CDOCNMBR")
  private String cdocnmbr;
  @Column(name = "CAMTDATE")
  private java.sql.Timestamp camtdate;
  @Column(name = "CAMPMTNM")
  private String campmtnm;
  @Column(name = "CHEKAMNT")
  private String chekamnt;
  @Column(name = "CHAMCBID")
  private String chamcbid;
  @Column(name = "CHEKDATE")
  private java.sql.Timestamp chekdate;
  @Column(name = "CAMPYNBR")
  private String campynbr;
  @Column(name = "CRCRDAMT")
  private String crcrdamt;
  @Column(name = "CCAMPYNM")
  private String ccampynm;
  @Column(name = "CHEKNMBR")
  private String cheknmbr;
  @Column(name = "CARDNAME")
  private String cardname;
  @Column(name = "CCRCTNUM")
  private String ccrctnum;
  @Column(name = "CRCARDDT")
  private java.sql.Timestamp crcarddt;
  @Column(name = "CURNCYID")
  private String curncyid;
  @Column(name = "CHEKBKID")
  private String chekbkid;
  @Column(name = "TRXDSCRN")
  private String trxdscrn;
  @Column(name = "UN1099AM")
  private String un1099Am;
  @Column(name = "TRDISAMT")
  private String trdisamt;
  @Column(name = "TAXAMNT")
  private String taxamnt;
  @Column(name = "FRTAMNT")
  private String frtamnt;
  @Column(name = "TEN99AMNT")
  private String ten99Amnt;
  @Column(name = "MSCCHAMT")
  private String mscchamt;
  @Column(name = "PORDNMBR")
  private String pordnmbr;
  @Column(name = "SHIPMTHD")
  private String shipmthd;
  @Column(name = "DISAMTAV")
  private String disamtav;
  @Column(name = "DISTKNAM")
  private String distknam;
  @Column(name = "APDSTKAM")
  private String apdstkam;
  @Column(name = "WROFAMNT")
  private String wrofamnt;
  @Column(name = "CURTRXAM")
  private String curtrxam;
  @Column(name = "TXENGCLD")
  private long txengcld;
  @Column(name = "PMWRKMSG")
  private String pmwrkmsg;
  @Column(name = "PMDSTMSG")
  private String pmdstmsg;
  @Column(name = "GSTDSAMT")
  private String gstdsamt;
  @Column(name = "PGRAMSBJ")
  private long pgramsbj;
  @Column(name = "PPSAMDED")
  private String ppsamded;
  @Column(name = "PPSTAXRT")
  private long ppstaxrt;
  @Column(name = "PSTGSTUS")
  private long pstgstus;
  @Column(name = "POSTED")
  private long posted;
  @Column(name = "APPLDAMT")
  private String appldamt;
  @Column(name = "VCHRNMBR")
  private String vchrnmbr;
  @Column(name = "CNTRLTYP")
  private long cntrltyp;
  @Column(name = "MODIFDT")
  private java.sql.Timestamp modifdt;
  @Column(name = "MDFUSRID")
  private String mdfusrid;
  @Column(name = "POSTEDDT")
  private java.sql.Timestamp posteddt;
  @Column(name = "PTDUSRID")
  private String ptdusrid;
  @Column(name = "NOTEINDX")
  private String noteindx;
  @Column(name = "BKTFRTAM")
  private String bktfrtam;
  @Column(name = "BKTMSCAM")
  private String bktmscam;
  @Column(name = "BKTPURAM")
  private String bktpuram;
  @Column(name = "PCHSCHID")
  private String pchschid;
  @Column(name = "FRTSCHID")
  private String frtschid;
  @Column(name = "MSCSCHID")
  private String mscschid;
  @Column(name = "PRINTED")
  private long printed;
  @Column(name = "PRCTDISC")
  private long prctdisc;
  @Column(name = "RETNAGAM")
  private String retnagam;
  @Column(name = "ICTRX")
  private long ictrx;
  @Column(name = "ICDISTS")
  private long icdists;
  @Column(name = "PMICMSGS")
  private String pmicmsgs;
  @Column(name = "Tax_Date")
  private java.sql.Timestamp taxDate;
  @Column(name = "PRCHDATE")
  private java.sql.Timestamp prchdate;
  @Column(name = "CORRCTN")
  private long corrctn;
  @Column(name = "SIMPLIFD")
  private long simplifd;
  @Column(name = "CORRNXST")
  private long corrnxst;
  @Column(name = "VCHRNCOR")
  private String vchrncor;
  @Column(name = "PMWRKMS2")
  private String pmwrkms2;
  @Column(name = "BNKRCAMT")
  private String bnkrcamt;
  @Column(name = "APLYWITH")
  private long aplywith;
  @Column(name = "Electronic")
  private long electronic;
  @Column(name = "ECTRX")
  private long ectrx;
  @Column(name = "DocPrinted")
  private long docPrinted;
  @Column(name = "TaxInvReqd")
  private long taxInvReqd;
  @Column(name = "BackoutTradeDisc")
  private String backoutTradeDisc;
  @Column(name = "CBVAT")
  private long cbvat;
  @Column(name = "TEN99TYPE")
  private long ten99Type;
  @Column(name = "TEN99BOXNUMBER")
  private long ten99Boxnumber;
  @Column(name = "Workflow_Status")
  private long workflowStatus;
  @Column(name = "InvoiceReceiptDate")
  private java.sql.Timestamp invoiceReceiptDate;
  @Column(name = "DEX_ROW_TS")
  private java.sql.Timestamp dexRowTs;
  @Id
  @Column(name = "DEX_ROW_ID")
  private long dexRowId;


  public String getBachnumb() {
    return bachnumb;
  }

  public void setBachnumb(String bachnumb) {
    this.bachnumb = bachnumb;
  }


  public String getBchsourc() {
    return bchsourc;
  }

  public void setBchsourc(String bchsourc) {
    this.bchsourc = bchsourc;
  }


  public String getVchnumwk() {
    return vchnumwk;
  }

  public void setVchnumwk(String vchnumwk) {
    this.vchnumwk = vchnumwk;
  }


  public String getVendorid() {
    return vendorid;
  }

  public void setVendorid(String vendorid) {
    this.vendorid = vendorid;
  }


  public String getDocnumbr() {
    return docnumbr;
  }

  public void setDocnumbr(String docnumbr) {
    this.docnumbr = docnumbr;
  }


  public long getDoctype() {
    return doctype;
  }

  public void setDoctype(long doctype) {
    this.doctype = doctype;
  }


  public String getSourcdoc() {
    return sourcdoc;
  }

  public void setSourcdoc(String sourcdoc) {
    this.sourcdoc = sourcdoc;
  }


  public String getDocamnt() {
    return docamnt;
  }

  public void setDocamnt(String docamnt) {
    this.docamnt = docamnt;
  }


  public java.sql.Timestamp getDocdate() {
    return docdate;
  }

  public void setDocdate(java.sql.Timestamp docdate) {
    this.docdate = docdate;
  }


  public java.sql.Timestamp getPstgdate() {
    return pstgdate;
  }

  public void setPstgdate(java.sql.Timestamp pstgdate) {
    this.pstgdate = pstgdate;
  }


  public String getVaddcdpr() {
    return vaddcdpr;
  }

  public void setVaddcdpr(String vaddcdpr) {
    this.vaddcdpr = vaddcdpr;
  }


  public String getVadcdtro() {
    return vadcdtro;
  }

  public void setVadcdtro(String vadcdtro) {
    this.vadcdtro = vadcdtro;
  }


  public String getPymtrmid() {
    return pymtrmid;
  }

  public void setPymtrmid(String pymtrmid) {
    this.pymtrmid = pymtrmid;
  }


  public String getTaxschid() {
    return taxschid;
  }

  public void setTaxschid(String taxschid) {
    this.taxschid = taxschid;
  }


  public java.sql.Timestamp getDuedate() {
    return duedate;
  }

  public void setDuedate(java.sql.Timestamp duedate) {
    this.duedate = duedate;
  }


  public String getDscdlram() {
    return dscdlram;
  }

  public void setDscdlram(String dscdlram) {
    this.dscdlram = dscdlram;
  }


  public java.sql.Timestamp getDiscdate() {
    return discdate;
  }

  public void setDiscdate(java.sql.Timestamp discdate) {
    this.discdate = discdate;
  }


  public String getPrchamnt() {
    return prchamnt;
  }

  public void setPrchamnt(String prchamnt) {
    this.prchamnt = prchamnt;
  }


  public String getChrgamnt() {
    return chrgamnt;
  }

  public void setChrgamnt(String chrgamnt) {
    this.chrgamnt = chrgamnt;
  }


  public String getCashamnt() {
    return cashamnt;
  }

  public void setCashamnt(String cashamnt) {
    this.cashamnt = cashamnt;
  }


  public String getCamcbkid() {
    return camcbkid;
  }

  public void setCamcbkid(String camcbkid) {
    this.camcbkid = camcbkid;
  }


  public String getCdocnmbr() {
    return cdocnmbr;
  }

  public void setCdocnmbr(String cdocnmbr) {
    this.cdocnmbr = cdocnmbr;
  }


  public java.sql.Timestamp getCamtdate() {
    return camtdate;
  }

  public void setCamtdate(java.sql.Timestamp camtdate) {
    this.camtdate = camtdate;
  }


  public String getCampmtnm() {
    return campmtnm;
  }

  public void setCampmtnm(String campmtnm) {
    this.campmtnm = campmtnm;
  }


  public String getChekamnt() {
    return chekamnt;
  }

  public void setChekamnt(String chekamnt) {
    this.chekamnt = chekamnt;
  }


  public String getChamcbid() {
    return chamcbid;
  }

  public void setChamcbid(String chamcbid) {
    this.chamcbid = chamcbid;
  }


  public java.sql.Timestamp getChekdate() {
    return chekdate;
  }

  public void setChekdate(java.sql.Timestamp chekdate) {
    this.chekdate = chekdate;
  }


  public String getCampynbr() {
    return campynbr;
  }

  public void setCampynbr(String campynbr) {
    this.campynbr = campynbr;
  }


  public String getCrcrdamt() {
    return crcrdamt;
  }

  public void setCrcrdamt(String crcrdamt) {
    this.crcrdamt = crcrdamt;
  }


  public String getCcampynm() {
    return ccampynm;
  }

  public void setCcampynm(String ccampynm) {
    this.ccampynm = ccampynm;
  }


  public String getCheknmbr() {
    return cheknmbr;
  }

  public void setCheknmbr(String cheknmbr) {
    this.cheknmbr = cheknmbr;
  }


  public String getCardname() {
    return cardname;
  }

  public void setCardname(String cardname) {
    this.cardname = cardname;
  }


  public String getCcrctnum() {
    return ccrctnum;
  }

  public void setCcrctnum(String ccrctnum) {
    this.ccrctnum = ccrctnum;
  }


  public java.sql.Timestamp getCrcarddt() {
    return crcarddt;
  }

  public void setCrcarddt(java.sql.Timestamp crcarddt) {
    this.crcarddt = crcarddt;
  }


  public String getCurncyid() {
    return curncyid;
  }

  public void setCurncyid(String curncyid) {
    this.curncyid = curncyid;
  }


  public String getChekbkid() {
    return chekbkid;
  }

  public void setChekbkid(String chekbkid) {
    this.chekbkid = chekbkid;
  }


  public String getTrxdscrn() {
    return trxdscrn;
  }

  public void setTrxdscrn(String trxdscrn) {
    this.trxdscrn = trxdscrn;
  }


  public String getUn1099Am() {
    return un1099Am;
  }

  public void setUn1099Am(String un1099Am) {
    this.un1099Am = un1099Am;
  }


  public String getTrdisamt() {
    return trdisamt;
  }

  public void setTrdisamt(String trdisamt) {
    this.trdisamt = trdisamt;
  }


  public String getTaxamnt() {
    return taxamnt;
  }

  public void setTaxamnt(String taxamnt) {
    this.taxamnt = taxamnt;
  }


  public String getFrtamnt() {
    return frtamnt;
  }

  public void setFrtamnt(String frtamnt) {
    this.frtamnt = frtamnt;
  }


  public String getTen99Amnt() {
    return ten99Amnt;
  }

  public void setTen99Amnt(String ten99Amnt) {
    this.ten99Amnt = ten99Amnt;
  }


  public String getMscchamt() {
    return mscchamt;
  }

  public void setMscchamt(String mscchamt) {
    this.mscchamt = mscchamt;
  }


  public String getPordnmbr() {
    return pordnmbr;
  }

  public void setPordnmbr(String pordnmbr) {
    this.pordnmbr = pordnmbr;
  }


  public String getShipmthd() {
    return shipmthd;
  }

  public void setShipmthd(String shipmthd) {
    this.shipmthd = shipmthd;
  }


  public String getDisamtav() {
    return disamtav;
  }

  public void setDisamtav(String disamtav) {
    this.disamtav = disamtav;
  }


  public String getDistknam() {
    return distknam;
  }

  public void setDistknam(String distknam) {
    this.distknam = distknam;
  }


  public String getApdstkam() {
    return apdstkam;
  }

  public void setApdstkam(String apdstkam) {
    this.apdstkam = apdstkam;
  }


  public String getWrofamnt() {
    return wrofamnt;
  }

  public void setWrofamnt(String wrofamnt) {
    this.wrofamnt = wrofamnt;
  }


  public String getCurtrxam() {
    return curtrxam;
  }

  public void setCurtrxam(String curtrxam) {
    this.curtrxam = curtrxam;
  }


  public long getTxengcld() {
    return txengcld;
  }

  public void setTxengcld(long txengcld) {
    this.txengcld = txengcld;
  }


  public String getPmwrkmsg() {
    return pmwrkmsg;
  }

  public void setPmwrkmsg(String pmwrkmsg) {
    this.pmwrkmsg = pmwrkmsg;
  }


  public String getPmdstmsg() {
    return pmdstmsg;
  }

  public void setPmdstmsg(String pmdstmsg) {
    this.pmdstmsg = pmdstmsg;
  }


  public String getGstdsamt() {
    return gstdsamt;
  }

  public void setGstdsamt(String gstdsamt) {
    this.gstdsamt = gstdsamt;
  }


  public long getPgramsbj() {
    return pgramsbj;
  }

  public void setPgramsbj(long pgramsbj) {
    this.pgramsbj = pgramsbj;
  }


  public String getPpsamded() {
    return ppsamded;
  }

  public void setPpsamded(String ppsamded) {
    this.ppsamded = ppsamded;
  }


  public long getPpstaxrt() {
    return ppstaxrt;
  }

  public void setPpstaxrt(long ppstaxrt) {
    this.ppstaxrt = ppstaxrt;
  }


  public long getPstgstus() {
    return pstgstus;
  }

  public void setPstgstus(long pstgstus) {
    this.pstgstus = pstgstus;
  }


  public long getPosted() {
    return posted;
  }

  public void setPosted(long posted) {
    this.posted = posted;
  }


  public String getAppldamt() {
    return appldamt;
  }

  public void setAppldamt(String appldamt) {
    this.appldamt = appldamt;
  }


  public String getVchrnmbr() {
    return vchrnmbr;
  }

  public void setVchrnmbr(String vchrnmbr) {
    this.vchrnmbr = vchrnmbr;
  }


  public long getCntrltyp() {
    return cntrltyp;
  }

  public void setCntrltyp(long cntrltyp) {
    this.cntrltyp = cntrltyp;
  }


  public java.sql.Timestamp getModifdt() {
    return modifdt;
  }

  public void setModifdt(java.sql.Timestamp modifdt) {
    this.modifdt = modifdt;
  }


  public String getMdfusrid() {
    return mdfusrid;
  }

  public void setMdfusrid(String mdfusrid) {
    this.mdfusrid = mdfusrid;
  }


  public java.sql.Timestamp getPosteddt() {
    return posteddt;
  }

  public void setPosteddt(java.sql.Timestamp posteddt) {
    this.posteddt = posteddt;
  }


  public String getPtdusrid() {
    return ptdusrid;
  }

  public void setPtdusrid(String ptdusrid) {
    this.ptdusrid = ptdusrid;
  }


  public String getNoteindx() {
    return noteindx;
  }

  public void setNoteindx(String noteindx) {
    this.noteindx = noteindx;
  }


  public String getBktfrtam() {
    return bktfrtam;
  }

  public void setBktfrtam(String bktfrtam) {
    this.bktfrtam = bktfrtam;
  }


  public String getBktmscam() {
    return bktmscam;
  }

  public void setBktmscam(String bktmscam) {
    this.bktmscam = bktmscam;
  }


  public String getBktpuram() {
    return bktpuram;
  }

  public void setBktpuram(String bktpuram) {
    this.bktpuram = bktpuram;
  }


  public String getPchschid() {
    return pchschid;
  }

  public void setPchschid(String pchschid) {
    this.pchschid = pchschid;
  }


  public String getFrtschid() {
    return frtschid;
  }

  public void setFrtschid(String frtschid) {
    this.frtschid = frtschid;
  }


  public String getMscschid() {
    return mscschid;
  }

  public void setMscschid(String mscschid) {
    this.mscschid = mscschid;
  }


  public long getPrinted() {
    return printed;
  }

  public void setPrinted(long printed) {
    this.printed = printed;
  }


  public long getPrctdisc() {
    return prctdisc;
  }

  public void setPrctdisc(long prctdisc) {
    this.prctdisc = prctdisc;
  }


  public String getRetnagam() {
    return retnagam;
  }

  public void setRetnagam(String retnagam) {
    this.retnagam = retnagam;
  }


  public long getIctrx() {
    return ictrx;
  }

  public void setIctrx(long ictrx) {
    this.ictrx = ictrx;
  }


  public long getIcdists() {
    return icdists;
  }

  public void setIcdists(long icdists) {
    this.icdists = icdists;
  }


  public String getPmicmsgs() {
    return pmicmsgs;
  }

  public void setPmicmsgs(String pmicmsgs) {
    this.pmicmsgs = pmicmsgs;
  }


  public java.sql.Timestamp getTaxDate() {
    return taxDate;
  }

  public void setTaxDate(java.sql.Timestamp taxDate) {
    this.taxDate = taxDate;
  }


  public java.sql.Timestamp getPrchdate() {
    return prchdate;
  }

  public void setPrchdate(java.sql.Timestamp prchdate) {
    this.prchdate = prchdate;
  }


  public long getCorrctn() {
    return corrctn;
  }

  public void setCorrctn(long corrctn) {
    this.corrctn = corrctn;
  }


  public long getSimplifd() {
    return simplifd;
  }

  public void setSimplifd(long simplifd) {
    this.simplifd = simplifd;
  }


  public long getCorrnxst() {
    return corrnxst;
  }

  public void setCorrnxst(long corrnxst) {
    this.corrnxst = corrnxst;
  }


  public String getVchrncor() {
    return vchrncor;
  }

  public void setVchrncor(String vchrncor) {
    this.vchrncor = vchrncor;
  }


  public String getPmwrkms2() {
    return pmwrkms2;
  }

  public void setPmwrkms2(String pmwrkms2) {
    this.pmwrkms2 = pmwrkms2;
  }


  public String getBnkrcamt() {
    return bnkrcamt;
  }

  public void setBnkrcamt(String bnkrcamt) {
    this.bnkrcamt = bnkrcamt;
  }


  public long getAplywith() {
    return aplywith;
  }

  public void setAplywith(long aplywith) {
    this.aplywith = aplywith;
  }


  public long getElectronic() {
    return electronic;
  }

  public void setElectronic(long electronic) {
    this.electronic = electronic;
  }


  public long getEctrx() {
    return ectrx;
  }

  public void setEctrx(long ectrx) {
    this.ectrx = ectrx;
  }


  public long getDocPrinted() {
    return docPrinted;
  }

  public void setDocPrinted(long docPrinted) {
    this.docPrinted = docPrinted;
  }


  public long getTaxInvReqd() {
    return taxInvReqd;
  }

  public void setTaxInvReqd(long taxInvReqd) {
    this.taxInvReqd = taxInvReqd;
  }


  public String getBackoutTradeDisc() {
    return backoutTradeDisc;
  }

  public void setBackoutTradeDisc(String backoutTradeDisc) {
    this.backoutTradeDisc = backoutTradeDisc;
  }


  public long getCbvat() {
    return cbvat;
  }

  public void setCbvat(long cbvat) {
    this.cbvat = cbvat;
  }


  public long getTen99Type() {
    return ten99Type;
  }

  public void setTen99Type(long ten99Type) {
    this.ten99Type = ten99Type;
  }


  public long getTen99Boxnumber() {
    return ten99Boxnumber;
  }

  public void setTen99Boxnumber(long ten99Boxnumber) {
    this.ten99Boxnumber = ten99Boxnumber;
  }


  public long getWorkflowStatus() {
    return workflowStatus;
  }

  public void setWorkflowStatus(long workflowStatus) {
    this.workflowStatus = workflowStatus;
  }


  public java.sql.Timestamp getInvoiceReceiptDate() {
    return invoiceReceiptDate;
  }

  public void setInvoiceReceiptDate(java.sql.Timestamp invoiceReceiptDate) {
    this.invoiceReceiptDate = invoiceReceiptDate;
  }


  public java.sql.Timestamp getDexRowTs() {
    return dexRowTs;
  }

  public void setDexRowTs(java.sql.Timestamp dexRowTs) {
    this.dexRowTs = dexRowTs;
  }


  public long getDexRowId() {
    return dexRowId;
  }

  public void setDexRowId(long dexRowId) {
    this.dexRowId = dexRowId;
  }

}
