package com.example.test.tablemodels;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;

@Entity
@Table(name = "PM20000", schema = "dbo")
public class Pm20000TableModel {

  @Column(name = "VCHRNMBR")
  private String vchrnmbr;
  @Column(name = "VENDORID")
  private String vendorid;
  @Column(name = "DOCTYPE")
  private long doctype;
  @Column(name = "DOCDATE")
  private java.sql.Timestamp docdate;
  @Column(name = "DOCNUMBR")
  private String docnumbr;
  @Column(name = "DOCAMNT")
  private String docamnt;
  @Column(name = "CURTRXAM")
  private String curtrxam;
  @Column(name = "DISTKNAM")
  private String distknam;
  @Column(name = "DISCAMNT")
  private String discamnt;
  @Column(name = "DSCDLRAM")
  private String dscdlram;
  @Column(name = "BACHNUMB")
  private String bachnumb;
  @Column(name = "TRXSORCE")
  private String trxsorce;
  @Column(name = "BCHSOURC")
  private String bchsourc;
  @Column(name = "DISCDATE")
  private java.sql.Timestamp discdate;
  @Column(name = "DUEDATE")
  private java.sql.Timestamp duedate;
  @Column(name = "PORDNMBR")
  private String pordnmbr;
  @Column(name = "TEN99AMNT")
  private String ten99Amnt;
  @Column(name = "WROFAMNT")
  private String wrofamnt;
  @Column(name = "DISAMTAV")
  private String disamtav;
  @Column(name = "TRXDSCRN")
  private String trxdscrn;
  @Column(name = "UN1099AM")
  private String un1099Am;
  @Column(name = "BKTPURAM")
  private String bktpuram;
  @Column(name = "BKTFRTAM")
  private String bktfrtam;
  @Column(name = "BKTMSCAM")
  private String bktmscam;
  @Column(name = "VOIDED")
  private long voided;
  @Column(name = "HOLD")
  private long hold;
  @Column(name = "CHEKBKID")
  private String chekbkid;
  @Column(name = "DINVPDOF")
  private java.sql.Timestamp dinvpdof;
  @Column(name = "PPSAMDED")
  private String ppsamded;
  @Column(name = "PPSTAXRT")
  private long ppstaxrt;
  @Column(name = "PGRAMSBJ")
  private long pgramsbj;
  @Column(name = "GSTDSAMT")
  private String gstdsamt;
  @Column(name = "POSTEDDT")
  private java.sql.Timestamp posteddt;
  @Column(name = "PTDUSRID")
  private String ptdusrid;
  @Column(name = "MODIFDT")
  private java.sql.Timestamp modifdt;
  @Column(name = "MDFUSRID")
  private String mdfusrid;
  @Column(name = "PYENTTYP")
  private long pyenttyp;
  @Column(name = "CARDNAME")
  private String cardname;
  @Column(name = "PRCHAMNT")
  private String prchamnt;
  @Column(name = "TRDISAMT")
  private String trdisamt;
  @Column(name = "MSCCHAMT")
  private String mscchamt;
  @Column(name = "FRTAMNT")
  private String frtamnt;
  @Column(name = "TAXAMNT")
  private String taxamnt;
  @Column(name = "TTLPYMTS")
  private String ttlpymts;
  @Column(name = "CURNCYID")
  private String curncyid;
  @Column(name = "PYMTRMID")
  private String pymtrmid;
  @Column(name = "SHIPMTHD")
  private String shipmthd;
  @Column(name = "TAXSCHID")
  private String taxschid;
  @Column(name = "PCHSCHID")
  private String pchschid;
  @Column(name = "FRTSCHID")
  private String frtschid;
  @Column(name = "MSCSCHID")
  private String mscschid;
  @Column(name = "PSTGDATE")
  private java.sql.Timestamp pstgdate;
  @Column(name = "DISAVTKN")
  private String disavtkn;
  @Column(name = "CNTRLTYP")
  private long cntrltyp;
  @Column(name = "NOTEINDX")
  private String noteindx;
  @Column(name = "PRCTDISC")
  private long prctdisc;
  @Column(name = "RETNAGAM")
  private String retnagam;
  @Column(name = "ICTRX")
  private long ictrx;
  @Column(name = "Tax_Date")
  private java.sql.Timestamp taxDate;
  @Column(name = "PRCHDATE")
  private java.sql.Timestamp prchdate;
  @Column(name = "CORRCTN")
  private long corrctn;
  @Column(name = "SIMPLIFD")
  private long simplifd;
  @Column(name = "BNKRCAMT")
  private String bnkrcamt;
  @Column(name = "APLYWITH")
  private long aplywith;
  @Column(name = "Electronic")
  private long electronic;
  @Column(name = "ECTRX")
  private long ectrx;
  @Column(name = "DocPrinted")
  private long docPrinted;
  @Column(name = "TaxInvReqd")
  private long taxInvReqd;
  @Column(name = "VNDCHKNM")
  private String vndchknm;
  @Column(name = "BackoutTradeDisc")
  private String backoutTradeDisc;
  @Column(name = "CBVAT")
  private long cbvat;
  @Column(name = "VADCDTRO")
  private String vadcdtro;
  @Column(name = "TEN99TYPE")
  private long ten99Type;
  @Column(name = "TEN99BOXNUMBER")
  private long ten99Boxnumber;
  @Column(name = "PONUMBER")
  private String ponumber;
  @Column(name = "Workflow_Status")
  private long workflowStatus;
  @Column(name = "InvoiceReceiptDate")
  private java.sql.Timestamp invoiceReceiptDate;
  @Column(name = "DEX_ROW_TS")
  private java.sql.Timestamp dexRowTs;
  @Id
  @Column(name = "DEX_ROW_ID")
  private long dexRowId;


  public String getVchrnmbr() {
    return vchrnmbr;
  }

  public void setVchrnmbr(String vchrnmbr) {
    this.vchrnmbr = vchrnmbr;
  }


  public String getVendorid() {
    return vendorid;
  }

  public void setVendorid(String vendorid) {
    this.vendorid = vendorid;
  }


  public long getDoctype() {
    return doctype;
  }

  public void setDoctype(long doctype) {
    this.doctype = doctype;
  }


  public java.sql.Timestamp getDocdate() {
    return docdate;
  }

  public void setDocdate(java.sql.Timestamp docdate) {
    this.docdate = docdate;
  }


  public String getDocnumbr() {
    return docnumbr;
  }

  public void setDocnumbr(String docnumbr) {
    this.docnumbr = docnumbr;
  }


  public String getDocamnt() {
    return docamnt;
  }

  public void setDocamnt(String docamnt) {
    this.docamnt = docamnt;
  }


  public String getCurtrxam() {
    return curtrxam;
  }

  public void setCurtrxam(String curtrxam) {
    this.curtrxam = curtrxam;
  }


  public String getDistknam() {
    return distknam;
  }

  public void setDistknam(String distknam) {
    this.distknam = distknam;
  }


  public String getDiscamnt() {
    return discamnt;
  }

  public void setDiscamnt(String discamnt) {
    this.discamnt = discamnt;
  }


  public String getDscdlram() {
    return dscdlram;
  }

  public void setDscdlram(String dscdlram) {
    this.dscdlram = dscdlram;
  }


  public String getBachnumb() {
    return bachnumb;
  }

  public void setBachnumb(String bachnumb) {
    this.bachnumb = bachnumb;
  }


  public String getTrxsorce() {
    return trxsorce;
  }

  public void setTrxsorce(String trxsorce) {
    this.trxsorce = trxsorce;
  }


  public String getBchsourc() {
    return bchsourc;
  }

  public void setBchsourc(String bchsourc) {
    this.bchsourc = bchsourc;
  }


  public java.sql.Timestamp getDiscdate() {
    return discdate;
  }

  public void setDiscdate(java.sql.Timestamp discdate) {
    this.discdate = discdate;
  }


  public java.sql.Timestamp getDuedate() {
    return duedate;
  }

  public void setDuedate(java.sql.Timestamp duedate) {
    this.duedate = duedate;
  }


  public String getPordnmbr() {
    return pordnmbr;
  }

  public void setPordnmbr(String pordnmbr) {
    this.pordnmbr = pordnmbr;
  }


  public String getTen99Amnt() {
    return ten99Amnt;
  }

  public void setTen99Amnt(String ten99Amnt) {
    this.ten99Amnt = ten99Amnt;
  }


  public String getWrofamnt() {
    return wrofamnt;
  }

  public void setWrofamnt(String wrofamnt) {
    this.wrofamnt = wrofamnt;
  }


  public String getDisamtav() {
    return disamtav;
  }

  public void setDisamtav(String disamtav) {
    this.disamtav = disamtav;
  }


  public String getTrxdscrn() {
    return trxdscrn;
  }

  public void setTrxdscrn(String trxdscrn) {
    this.trxdscrn = trxdscrn;
  }


  public String getUn1099Am() {
    return un1099Am;
  }

  public void setUn1099Am(String un1099Am) {
    this.un1099Am = un1099Am;
  }


  public String getBktpuram() {
    return bktpuram;
  }

  public void setBktpuram(String bktpuram) {
    this.bktpuram = bktpuram;
  }


  public String getBktfrtam() {
    return bktfrtam;
  }

  public void setBktfrtam(String bktfrtam) {
    this.bktfrtam = bktfrtam;
  }


  public String getBktmscam() {
    return bktmscam;
  }

  public void setBktmscam(String bktmscam) {
    this.bktmscam = bktmscam;
  }


  public long getVoided() {
    return voided;
  }

  public void setVoided(long voided) {
    this.voided = voided;
  }


  public long getHold() {
    return hold;
  }

  public void setHold(long hold) {
    this.hold = hold;
  }


  public String getChekbkid() {
    return chekbkid;
  }

  public void setChekbkid(String chekbkid) {
    this.chekbkid = chekbkid;
  }


  public java.sql.Timestamp getDinvpdof() {
    return dinvpdof;
  }

  public void setDinvpdof(java.sql.Timestamp dinvpdof) {
    this.dinvpdof = dinvpdof;
  }


  public String getPpsamded() {
    return ppsamded;
  }

  public void setPpsamded(String ppsamded) {
    this.ppsamded = ppsamded;
  }


  public long getPpstaxrt() {
    return ppstaxrt;
  }

  public void setPpstaxrt(long ppstaxrt) {
    this.ppstaxrt = ppstaxrt;
  }


  public long getPgramsbj() {
    return pgramsbj;
  }

  public void setPgramsbj(long pgramsbj) {
    this.pgramsbj = pgramsbj;
  }


  public String getGstdsamt() {
    return gstdsamt;
  }

  public void setGstdsamt(String gstdsamt) {
    this.gstdsamt = gstdsamt;
  }


  public java.sql.Timestamp getPosteddt() {
    return posteddt;
  }

  public void setPosteddt(java.sql.Timestamp posteddt) {
    this.posteddt = posteddt;
  }


  public String getPtdusrid() {
    return ptdusrid;
  }

  public void setPtdusrid(String ptdusrid) {
    this.ptdusrid = ptdusrid;
  }


  public java.sql.Timestamp getModifdt() {
    return modifdt;
  }

  public void setModifdt(java.sql.Timestamp modifdt) {
    this.modifdt = modifdt;
  }


  public String getMdfusrid() {
    return mdfusrid;
  }

  public void setMdfusrid(String mdfusrid) {
    this.mdfusrid = mdfusrid;
  }


  public long getPyenttyp() {
    return pyenttyp;
  }

  public void setPyenttyp(long pyenttyp) {
    this.pyenttyp = pyenttyp;
  }


  public String getCardname() {
    return cardname;
  }

  public void setCardname(String cardname) {
    this.cardname = cardname;
  }


  public String getPrchamnt() {
    return prchamnt;
  }

  public void setPrchamnt(String prchamnt) {
    this.prchamnt = prchamnt;
  }


  public String getTrdisamt() {
    return trdisamt;
  }

  public void setTrdisamt(String trdisamt) {
    this.trdisamt = trdisamt;
  }


  public String getMscchamt() {
    return mscchamt;
  }

  public void setMscchamt(String mscchamt) {
    this.mscchamt = mscchamt;
  }


  public String getFrtamnt() {
    return frtamnt;
  }

  public void setFrtamnt(String frtamnt) {
    this.frtamnt = frtamnt;
  }


  public String getTaxamnt() {
    return taxamnt;
  }

  public void setTaxamnt(String taxamnt) {
    this.taxamnt = taxamnt;
  }


  public String getTtlpymts() {
    return ttlpymts;
  }

  public void setTtlpymts(String ttlpymts) {
    this.ttlpymts = ttlpymts;
  }


  public String getCurncyid() {
    return curncyid;
  }

  public void setCurncyid(String curncyid) {
    this.curncyid = curncyid;
  }


  public String getPymtrmid() {
    return pymtrmid;
  }

  public void setPymtrmid(String pymtrmid) {
    this.pymtrmid = pymtrmid;
  }


  public String getShipmthd() {
    return shipmthd;
  }

  public void setShipmthd(String shipmthd) {
    this.shipmthd = shipmthd;
  }


  public String getTaxschid() {
    return taxschid;
  }

  public void setTaxschid(String taxschid) {
    this.taxschid = taxschid;
  }


  public String getPchschid() {
    return pchschid;
  }

  public void setPchschid(String pchschid) {
    this.pchschid = pchschid;
  }


  public String getFrtschid() {
    return frtschid;
  }

  public void setFrtschid(String frtschid) {
    this.frtschid = frtschid;
  }


  public String getMscschid() {
    return mscschid;
  }

  public void setMscschid(String mscschid) {
    this.mscschid = mscschid;
  }


  public java.sql.Timestamp getPstgdate() {
    return pstgdate;
  }

  public void setPstgdate(java.sql.Timestamp pstgdate) {
    this.pstgdate = pstgdate;
  }


  public String getDisavtkn() {
    return disavtkn;
  }

  public void setDisavtkn(String disavtkn) {
    this.disavtkn = disavtkn;
  }


  public long getCntrltyp() {
    return cntrltyp;
  }

  public void setCntrltyp(long cntrltyp) {
    this.cntrltyp = cntrltyp;
  }


  public String getNoteindx() {
    return noteindx;
  }

  public void setNoteindx(String noteindx) {
    this.noteindx = noteindx;
  }


  public long getPrctdisc() {
    return prctdisc;
  }

  public void setPrctdisc(long prctdisc) {
    this.prctdisc = prctdisc;
  }


  public String getRetnagam() {
    return retnagam;
  }

  public void setRetnagam(String retnagam) {
    this.retnagam = retnagam;
  }


  public long getIctrx() {
    return ictrx;
  }

  public void setIctrx(long ictrx) {
    this.ictrx = ictrx;
  }


  public java.sql.Timestamp getTaxDate() {
    return taxDate;
  }

  public void setTaxDate(java.sql.Timestamp taxDate) {
    this.taxDate = taxDate;
  }


  public java.sql.Timestamp getPrchdate() {
    return prchdate;
  }

  public void setPrchdate(java.sql.Timestamp prchdate) {
    this.prchdate = prchdate;
  }


  public long getCorrctn() {
    return corrctn;
  }

  public void setCorrctn(long corrctn) {
    this.corrctn = corrctn;
  }


  public long getSimplifd() {
    return simplifd;
  }

  public void setSimplifd(long simplifd) {
    this.simplifd = simplifd;
  }


  public String getBnkrcamt() {
    return bnkrcamt;
  }

  public void setBnkrcamt(String bnkrcamt) {
    this.bnkrcamt = bnkrcamt;
  }


  public long getAplywith() {
    return aplywith;
  }

  public void setAplywith(long aplywith) {
    this.aplywith = aplywith;
  }


  public long getElectronic() {
    return electronic;
  }

  public void setElectronic(long electronic) {
    this.electronic = electronic;
  }


  public long getEctrx() {
    return ectrx;
  }

  public void setEctrx(long ectrx) {
    this.ectrx = ectrx;
  }


  public long getDocPrinted() {
    return docPrinted;
  }

  public void setDocPrinted(long docPrinted) {
    this.docPrinted = docPrinted;
  }


  public long getTaxInvReqd() {
    return taxInvReqd;
  }

  public void setTaxInvReqd(long taxInvReqd) {
    this.taxInvReqd = taxInvReqd;
  }


  public String getVndchknm() {
    return vndchknm;
  }

  public void setVndchknm(String vndchknm) {
    this.vndchknm = vndchknm;
  }


  public String getBackoutTradeDisc() {
    return backoutTradeDisc;
  }

  public void setBackoutTradeDisc(String backoutTradeDisc) {
    this.backoutTradeDisc = backoutTradeDisc;
  }


  public long getCbvat() {
    return cbvat;
  }

  public void setCbvat(long cbvat) {
    this.cbvat = cbvat;
  }


  public String getVadcdtro() {
    return vadcdtro;
  }

  public void setVadcdtro(String vadcdtro) {
    this.vadcdtro = vadcdtro;
  }


  public long getTen99Type() {
    return ten99Type;
  }

  public void setTen99Type(long ten99Type) {
    this.ten99Type = ten99Type;
  }


  public long getTen99Boxnumber() {
    return ten99Boxnumber;
  }

  public void setTen99Boxnumber(long ten99Boxnumber) {
    this.ten99Boxnumber = ten99Boxnumber;
  }


  public String getPonumber() {
    return ponumber;
  }

  public void setPonumber(String ponumber) {
    this.ponumber = ponumber;
  }


  public long getWorkflowStatus() {
    return workflowStatus;
  }

  public void setWorkflowStatus(long workflowStatus) {
    this.workflowStatus = workflowStatus;
  }


  public java.sql.Timestamp getInvoiceReceiptDate() {
    return invoiceReceiptDate;
  }

  public void setInvoiceReceiptDate(java.sql.Timestamp invoiceReceiptDate) {
    this.invoiceReceiptDate = invoiceReceiptDate;
  }


  public java.sql.Timestamp getDexRowTs() {
    return dexRowTs;
  }

  public void setDexRowTs(java.sql.Timestamp dexRowTs) {
    this.dexRowTs = dexRowTs;
  }


  public long getDexRowId() {
    return dexRowId;
  }

  public void setDexRowId(long dexRowId) {
    this.dexRowId = dexRowId;
  }

}
