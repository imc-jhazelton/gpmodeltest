package com.example.test;

import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface MasterRepository {

	List<?> findAllByVendorid(String vendorid);
	
	List<?> findAllByVendoridAndDoctypeLessThan(String vendorid, long doctype);
}
