package com.example.test;

import java.util.List;

import com.example.test.tablemodels.Pm20000TableModel;

public class AllCompanyAPResponse {

	List<Pm20000TableModel> ais;
	List<Pm20000TableModel> dnj;
	List<Pm20000TableModel> gis;
	List<Pm20000TableModel> glso;
	List<Pm20000TableModel> hmis;
	List<Pm20000TableModel> imc;
	List<Pm20000TableModel> pds;
	List<Pm20000TableModel> pts;

	public List<Pm20000TableModel> getAis() {
		return ais;
	}

	public void setAis(List<Pm20000TableModel> ais) {
		this.ais = ais;
	}

	public List<Pm20000TableModel> getDnj() {
		return dnj;
	}

	public void setDnj(List<Pm20000TableModel> dnj) {
		this.dnj = dnj;
	}

	public List<Pm20000TableModel> getGis() {
		return gis;
	}

	public void setGis(List<Pm20000TableModel> gis) {
		this.gis = gis;
	}

	public List<Pm20000TableModel> getGlso() {
		return glso;
	}

	public void setGlso(List<Pm20000TableModel> glso) {
		this.glso = glso;
	}

	public List<Pm20000TableModel> getHmis() {
		return hmis;
	}

	public void setHmis(List<Pm20000TableModel> hmis) {
		this.hmis = hmis;
	}

	public List<Pm20000TableModel> getImc() {
		return imc;
	}

	public void setImc(List<Pm20000TableModel> imc) {
		this.imc = imc;
	}

	public List<Pm20000TableModel> getPds() {
		return pds;
	}

	public void setPds(List<Pm20000TableModel> pds) {
		this.pds = pds;
	}

	public List<Pm20000TableModel> getPts() {
		return pts;
	}

	public void setPts(List<Pm20000TableModel> pts) {
		this.pts = pts;
	}

}
