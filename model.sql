USE [IMC]
GO

/****** Object:  Table [dbo].[PM20000]    Script Date: 10/17/2019 5:53:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PM20000](
	[VCHRNMBR] [char](21) NOT NULL,
	[VENDORID] [char](15) NOT NULL,
	[DOCTYPE] [smallint] NOT NULL,
	[DOCDATE] [datetime] NOT NULL,
	[DOCNUMBR] [char](21) NOT NULL,
	[DOCAMNT] [numeric](19, 5) NOT NULL,
	[CURTRXAM] [numeric](19, 5) NOT NULL,
	[DISTKNAM] [numeric](19, 5) NOT NULL,
	[DISCAMNT] [numeric](19, 5) NOT NULL,
	[DSCDLRAM] [numeric](19, 5) NOT NULL,
	[BACHNUMB] [char](15) NOT NULL,
	[TRXSORCE] [char](13) NOT NULL,
	[BCHSOURC] [char](15) NOT NULL,
	[DISCDATE] [datetime] NOT NULL,
	[DUEDATE] [datetime] NOT NULL,
	[PORDNMBR] [char](21) NOT NULL,
	[TEN99AMNT] [numeric](19, 5) NOT NULL,
	[WROFAMNT] [numeric](19, 5) NOT NULL,
	[DISAMTAV] [numeric](19, 5) NOT NULL,
	[TRXDSCRN] [char](31) NOT NULL,
	[UN1099AM] [numeric](19, 5) NOT NULL,
	[BKTPURAM] [numeric](19, 5) NOT NULL,
	[BKTFRTAM] [numeric](19, 5) NOT NULL,
	[BKTMSCAM] [numeric](19, 5) NOT NULL,
	[VOIDED] [tinyint] NOT NULL,
	[HOLD] [tinyint] NOT NULL,
	[CHEKBKID] [char](15) NOT NULL,
	[DINVPDOF] [datetime] NOT NULL,
	[PPSAMDED] [numeric](19, 5) NOT NULL,
	[PPSTAXRT] [smallint] NOT NULL,
	[PGRAMSBJ] [smallint] NOT NULL,
	[GSTDSAMT] [numeric](19, 5) NOT NULL,
	[POSTEDDT] [datetime] NOT NULL,
	[PTDUSRID] [char](15) NOT NULL,
	[MODIFDT] [datetime] NOT NULL,
	[MDFUSRID] [char](15) NOT NULL,
	[PYENTTYP] [smallint] NOT NULL,
	[CARDNAME] [char](15) NOT NULL,
	[PRCHAMNT] [numeric](19, 5) NOT NULL,
	[TRDISAMT] [numeric](19, 5) NOT NULL,
	[MSCCHAMT] [numeric](19, 5) NOT NULL,
	[FRTAMNT] [numeric](19, 5) NOT NULL,
	[TAXAMNT] [numeric](19, 5) NOT NULL,
	[TTLPYMTS] [numeric](19, 5) NOT NULL,
	[CURNCYID] [char](15) NOT NULL,
	[PYMTRMID] [char](21) NOT NULL,
	[SHIPMTHD] [char](15) NOT NULL,
	[TAXSCHID] [char](15) NOT NULL,
	[PCHSCHID] [char](15) NOT NULL,
	[FRTSCHID] [char](15) NOT NULL,
	[MSCSCHID] [char](15) NOT NULL,
	[PSTGDATE] [datetime] NOT NULL,
	[DISAVTKN] [numeric](19, 5) NOT NULL,
	[CNTRLTYP] [smallint] NOT NULL,
	[NOTEINDX] [numeric](19, 5) NOT NULL,
	[PRCTDISC] [smallint] NOT NULL,
	[RETNAGAM] [numeric](19, 5) NOT NULL,
	[ICTRX] [tinyint] NOT NULL,
	[Tax_Date] [datetime] NOT NULL,
	[PRCHDATE] [datetime] NOT NULL,
	[CORRCTN] [tinyint] NOT NULL,
	[SIMPLIFD] [tinyint] NOT NULL,
	[BNKRCAMT] [numeric](19, 5) NOT NULL,
	[APLYWITH] [tinyint] NOT NULL,
	[Electronic] [tinyint] NOT NULL,
	[ECTRX] [tinyint] NOT NULL,
	[DocPrinted] [tinyint] NOT NULL,
	[TaxInvReqd] [tinyint] NOT NULL,
	[VNDCHKNM] [char](65) NOT NULL,
	[BackoutTradeDisc] [numeric](19, 5) NOT NULL,
	[CBVAT] [tinyint] NOT NULL,
	[VADCDTRO] [char](15) NOT NULL,
	[TEN99TYPE] [smallint] NOT NULL,
	[TEN99BOXNUMBER] [smallint] NOT NULL,
	[PONUMBER] [char](17) NOT NULL,
	[Workflow_Status] [smallint] NOT NULL,
	[InvoiceReceiptDate] [datetime] NOT NULL,
	[DEX_ROW_TS] [datetime] NOT NULL,
	[DEX_ROW_ID] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PKPM20000] PRIMARY KEY NONCLUSTERED 
(
	[DOCTYPE] ASC,
	[VCHRNMBR] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PM20000] ADD  DEFAULT (getutcdate()) FOR [DEX_ROW_TS]
GO

ALTER TABLE [dbo].[PM20000]  WITH CHECK ADD CHECK  ((datepart(hour,[DINVPDOF])=(0) AND datepart(minute,[DINVPDOF])=(0) AND datepart(second,[DINVPDOF])=(0) AND datepart(millisecond,[DINVPDOF])=(0)))
GO

ALTER TABLE [dbo].[PM20000]  WITH CHECK ADD CHECK  ((datepart(hour,[DISCDATE])=(0) AND datepart(minute,[DISCDATE])=(0) AND datepart(second,[DISCDATE])=(0) AND datepart(millisecond,[DISCDATE])=(0)))
GO

ALTER TABLE [dbo].[PM20000]  WITH CHECK ADD CHECK  ((datepart(hour,[DOCDATE])=(0) AND datepart(minute,[DOCDATE])=(0) AND datepart(second,[DOCDATE])=(0) AND datepart(millisecond,[DOCDATE])=(0)))
GO

ALTER TABLE [dbo].[PM20000]  WITH CHECK ADD CHECK  ((datepart(hour,[DUEDATE])=(0) AND datepart(minute,[DUEDATE])=(0) AND datepart(second,[DUEDATE])=(0) AND datepart(millisecond,[DUEDATE])=(0)))
GO

ALTER TABLE [dbo].[PM20000]  WITH CHECK ADD CHECK  ((datepart(hour,[InvoiceReceiptDate])=(0) AND datepart(minute,[InvoiceReceiptDate])=(0) AND datepart(second,[InvoiceReceiptDate])=(0) AND datepart(millisecond,[InvoiceReceiptDate])=(0)))
GO

ALTER TABLE [dbo].[PM20000]  WITH CHECK ADD CHECK  ((datepart(hour,[MODIFDT])=(0) AND datepart(minute,[MODIFDT])=(0) AND datepart(second,[MODIFDT])=(0) AND datepart(millisecond,[MODIFDT])=(0)))
GO

ALTER TABLE [dbo].[PM20000]  WITH CHECK ADD CHECK  ((datepart(hour,[POSTEDDT])=(0) AND datepart(minute,[POSTEDDT])=(0) AND datepart(second,[POSTEDDT])=(0) AND datepart(millisecond,[POSTEDDT])=(0)))
GO

ALTER TABLE [dbo].[PM20000]  WITH CHECK ADD CHECK  ((datepart(hour,[PRCHDATE])=(0) AND datepart(minute,[PRCHDATE])=(0) AND datepart(second,[PRCHDATE])=(0) AND datepart(millisecond,[PRCHDATE])=(0)))
GO

ALTER TABLE [dbo].[PM20000]  WITH CHECK ADD CHECK  ((datepart(hour,[PSTGDATE])=(0) AND datepart(minute,[PSTGDATE])=(0) AND datepart(second,[PSTGDATE])=(0) AND datepart(millisecond,[PSTGDATE])=(0)))
GO

ALTER TABLE [dbo].[PM20000]  WITH CHECK ADD CHECK  ((datepart(hour,[Tax_Date])=(0) AND datepart(minute,[Tax_Date])=(0) AND datepart(second,[Tax_Date])=(0) AND datepart(millisecond,[Tax_Date])=(0)))
GO

